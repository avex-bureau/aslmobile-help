---
layout: page
title:  "Safety Reports"
lang: en
ref: safety-form
---

## Safety Report Form

To open the menu with the available Report Forms, click the **+** sign in the lower right corner of the screen (you can hide the menu that appears by clicking the **X** sign).

To prepare a Safety Report, click **SR** sign  and Safety Report Form will appear.

Safety Report Form allows you to enter the following information:

**Mandatory information** (marked with a red asterisk).
- **Date** of occurrence or hazard identification (report sending date and time will be attached  automatically).
- **Full name** or **ID number** of the User (ID number - if the airline has decided not to place personal data of employees in the ASL database).
- **Report type** (anonymous, confidential, voluntary, mandatory). When choosing an anonymous report type, Uzer's full name or ID number is deleted from the form and eliminated from the Report.
- **Department** where the User is employed.
- **Description** containing the text of the Report (no more than 2000 characters).

**Feedback**  
If the User considers it necessary to get reply or comment on his Report, the User can choose one or both feedback options.  
For anonymous Reports, the response or comment does not violate the anonymity of the User.

> The app will receive feedback notifications until you disconnect from the server in the settings.

**Additional Info** (optional)
- **Aircraft** tail number
- **Flight** number
- **Airport**
- **Aircraft System**

**Classification** (optional) allows to describe cause-effect relationships by indicating:
- **Incident**, if occurred.
- **Consequence**, related with the occurrence.
- **Area of deficiency** - operating area related to the occurrence or identified hazards.
- **Hazard Groups** - identified hazard groups.
- **Events** - immediate causes that have led or may lead to Incidents.
- **Pre-Events** - immediate causes that have led or may lead to Events.
- **Factors** - first manifestations of hazards (hazardous factors) that led or may lead to Pre-Events and Events.

**Attachments** (optional)
Up to three files of any format may be attached to the Report.
Large files processing and saving may take some time.

> The total size of attachments should not exceed 30MB

## Preparing a Safety Report

**Preparing and saving a Safety Report on a mobile device does not require an Internet connection.** 

It is most convenient to enter information in the order as presented in the Safety Report Form:
- **Enter mandatory information**, by clicking corresponding lines and choosing the necessary data from the opened menus.  
Mandatory **Description** must be accurate and sufficient for understanding.  
It is not the best practice to enter information that is contained in other fields of the form, however, you can specify the phase of the flight, as well as the airport or flight number, if they are not contained in the corresponding menus.  
**The user shall not enter the personal information of third parties in the Report text.**

- **Decide on the need for feedback** to your report

- If an incident occurs, click on the line **Incident** and make a selection in the list that opens. For your convenience, you can use the ***Search*** window to search for the incident by name.

- If you know actual consequences of a case in operation, click on the line **Consequence** and select the most significant consequence in the list that opens. For your convenience, you can use the ***Search*** window to search for the consequence by name.

- It is advisable to indicate the operationg area in which a hazard or deficiency was identified.  
To do this, click on the line **Area of deficiency** and make a selection in the list that opens.  
The selection of the area will automatically filter assosiated Events, Pre-Events and Factors, simplifying preparation of the report. In addition, this will allow the airline to identify deficient operating areas.  
If occurrence or reported hazard is related with several areas, leave this field blank.  

- It is advisable to indicate Hazard groups.  
To do this, click on the line **Hazard groups** and select one or more groups in the list that opens. After selecting hazard groups, click on the arrow in the upper left corner of the screen.  
For your convenience, all hazard groups are divided into four classes - Organizational, Environment, Human Factor and Technical. You can also use the ***Search*** window to search for hazard group by name.  
The choice of hazard groups will automatically filter assosiated Factors, simplifying the preparation of the report. In addition, this will allow the airline to determine the degree of impact of hazards on operations.

- It is advisable to indicate Events associated with a reported case in operation.  
To do this, click on the line **Events** and select one or more events in the list that opens. After selecting events, click on the arrow in the upper left corner of the screen.  
For your convenience, the list of events will be filtered according to selected Area of deficiency. You can also use the ***Search*** window to search for event by name.

- It is advisable to indicate Pre-Events associated with a reported case in operation.  
To do this, click on the line **Pre-Events** and select one or more pre-events in the list that opens. After selecting pre-events, click on the arrow in the upper left corner of the screen.  
For your convenience, the list of pre-events will be filtered according to selected Area of deficiency. You can also use the ***Search*** window to search for pre-event by name.

- It is advisable to indicate Factors associated with a reported case in operation.  
To do this, click on the line **Factors** and select one or more factors in the list that opens. After selecting factors, click on the arrow in the upper left corner of the screen.  
For your convenience, the list of factors will be filtered according to selected Area of deficiency and Hazard groups. You can also use the ***Search*** window to search for factor by name.

> **Important Note:**  
> Correct selection of Events, Pre-Events, Factors and Hazard Groups will allow the airline to determine the cause-and-effect models of operational cases and obtain their adequate forecast.

- If necessary, information in Description may be supplemented in the files attached to the report. This can be a text document, photo, video, sound message, etc.  
To attach a file, click on the line **File** and use the menu that opens to select a file stored on your mobile device.  
Any selected file can be deleted by clicking the **X** sign opposite the file name.

### After filling out Safety Report Form, do not forget to save the report on your mobile device by clicking the "Save" button.

The saved report can be deleted, edited before sending, or sent to the airline's ASL website:
- Go to the main page of the Application.
- To delete a report from a mobile device, click ***Delete***.
- To edit saved report, click ***Edit***.
- To send saved report to the airline's ASL website, please ensure internet connection and click ***Send***.