---
layout: page
title:  "Fatigue form"
lang: en
ref: fatigue-form
---

## Open Fatigue Report Form

To prepre a Fatigue Report it is necessary to fill Fatigue Report Form. If this form is available for you - press on "envelope" at the right lower corner of the screen. You will see a menue and "Fatigue Report" shall be selected.

![](/media/img/2020-04-11_22-29.png)

## Filling out the Fatigue Report Form

Fatigue Report Form has mandatory and optional information fields. Mandatory fields are marked with a red asterisk.

To fill in the time fields, select hours and minutes by scrolling up or down.

![](/media/img/help_set_time_android.png)

Upon filling out a report, press button "Save". If any mandatory field was not filled, a notification will pop-up.
