---
layout: page
title:  "How to log in to the server and configure the Application Settings"
lang: en
ref: settings
---
## Log in to the server

**After downloading the Application to the mobile device**, you need to log in by performing the following steps:
- Enter the airline ASL server address.
- Enter Usrename.
- Enter User password.
- Click "Sign in".

**If all the data was entered correctly and the device has a stable Internet connection, you will be transferred to the "Settings" page, and the notification "Database updated" will appear at the bottom of the screen**

## Configuring the Application Settings

**After log in to the server** you need to configure the Application Settings by following these steps:
- Select the Application language (Russian or English).
- Select the Application background (the default is a light theme).
- Select the airline division in which the User works (*the selected division is automatically inserted into the messages and determines the possibility to report in flight fsatique*).
- Enter the name of the User (for airlines operating in accordance with the requirements of GDPR, this field is hidden).
- Enter the personal ID number that is assigned by the airline (for information on the personal ID number, check with the Safety Manager).
- Click "Save"

**If all the actions are performed correctly and the Internet connection is stable, the notification “Settings saved” will appear at the bottom of the screen.**

### Now the application is configured and ready to use. Go to the Home page by clicking on the arrow in the upper left corner of the screen.

**Notes:**

1. The Application settings can be changed by the User at any time. To do this, on the Home page, click on the icon ... in the upper left corner of the screen and in the side panel, go to the *Settings* page. After the changes, save the new settings.

2. The server log in parameters can be changed by the User. For this:
    - Go to the *Settings* page.
    - Click on the angle bracket in the section *Connection to the server*.
    - Choose one of the options:
	    - "DELETE CONNECTION" (Disconnect from server, delete all data in app and stop feedback for previous messages).
	    - "Reconnect" (change login and password without losing feedback for previously submitted messages).