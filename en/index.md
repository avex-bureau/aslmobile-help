---
layout: default
lang: en
ref: home
---

<div class="home">
  {% assign pages=site.pages | where:"lang", page.lang %}
  <ul class="post-list">
    {% for page in pages %}
      {% if page.ref != 'home' %}
        <li>
          <a class="post-link" href="{{ page.url | prepend: site.baseurl }}">{{ page.title }}</a>
        </li>
      {% endif %}
    {% endfor %}
  </ul>
</div>
