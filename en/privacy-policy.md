---
layout: page
title:  "Privacy policy"
lang: en
ref: privacy-policy
---

## General information

AVEX Bureau created the ASL-mobile application (hereinafter - the Application) as a free application. This service is provided by AVEX Bureau for free and is intended to be used as is.

This page informs visitors about how exactly the data collected by the Application about users, their devices and third parties is used. Please read this Policy.

If you decide to use the Application, you agree to the collection and use of information in accordance with this policy. The personal information we collect is used to provide and improve the Application. We will not use or transfer your information to anyone, except as described in this Privacy Policy.

Using the Application means the User’s unconditional consent to this Policy and specified conditions for processing of  personal information. In case of disagreement with these conditions and Policy, the User should refrain from using the Application.

The user must timely update the ASL-mobile application in accordance with the recommendations of the Application Developer received by him. When transferring a mobile device for permanent use to another user, it is necessary to remove the Application from the transferred mobile device.

The terms used in this Privacy Policy have the same meanings as in "Terms and Conditions of the Application", which are available on the ASL mobile phone, unless otherwise specified in this Privacy Policy.

## Information Collection and Use

Personal information, which alone or in combination with other information, allows the User identification is limited to the user's name and email address. For Users who are subject to the DGPR rules, e-mail should not be indicated, and instead of the last name, a unique code assigned by the employer should be indicated. In this case, compliance with the DGPR requirements regarding message processing is the responsibility of the User’s employer.

Other information - information that does not disclose the user's identity, either by itself or in combination with other information received, is limited to an automatically assigned token, which allows the user to receive push notifications in the mobile device and the Application.

Data sent from the Application is used only by the employer of the User and solely with the purpose of safety management.

**In the message text, the User should not mention the personal data of third parties**

The application does not collect or store information about the user's device and data on the use of the application or geolocation. However, the Application uses third-party services which may collect information that identifies the user.

Link to privacy policy of third party service providers used by the Application:

*   [Google Play Services](https://www.google.com/policies/privacy/)
*   [Google Analytics for Firebase](https://firebase.google.com/policies/analytics)
*   [Firebase Crashlytics](https://firebase.google.com/support/privacy/)

The Application does not monitor User messages, but allows him to store and delete information about sent messages and received push notifications.

## Log Data

We want to inform you that whenever you use the Application, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing the Application, the time and date of your use of the Application, and other statistics.

## Cookies

Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory.

The Application does not use these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of the Application.

## Service Providers

We may employ third-party companies and individuals due to the following purposes:

*   To perform Service-related services; or
*   To assist us in analyzing how our Service is used.

We want to inform users of the Application that these third parties have access to your Personal Information, but are obligated not to disclose or use the information for any other purpose.

## Security

We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.

Keep passwords from your accounts in a safe place and do not disclose them to third parties. In case of loss of a mobile device with the Application downloaded, unauthorized use of your password or other breach of security, immediately notify your Employer.

## Links to Other Sites

The Application may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. we have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.

## Children’s Privacy

The Application is not intended for use by children. Users must control the use of the application by their children and not give them their username and password.

## Changes to This Privacy Policy

We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page.

This policy is effective as of 2020-04-11

## Contact Us

If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at avex@avex.pro.

This privacy policy page was created at [privacypolicytemplate.net](https://privacypolicytemplate.net) and modified/generated wit a help of [App Privacy Policy Generator](https://app-privacy-policy-generator.firebaseapp.com/)