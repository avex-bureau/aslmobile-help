---
layout: page
title: "Create report from template"
lang: en
ref: to-template
---



Often there is a need to create several reports of the same type. For example, in one flight several events occurred or there were several remarks on the operation of aircraft systems. In this case, it is not necessary to fill out an empty form each time. Just click on the "Template" button after the first message.

![](/media/img/2020-04-11_22-40.png)

After that the form with pre-filled fields will open:

- Date;
- Report type;
- Name
- Department (unit, etc.);
- Airport;
- Flight number;
- Aircraft Registration Number
- Feedback option.

