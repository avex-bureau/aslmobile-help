---
layout: page
title:  "Report types"
lang: en
ref: report-types
---

## Anonymous

When you select an anonymous message, the field "Full name" is cleared and blocked.
Ensures an employee’s right to anonymous reporting of a situation that affects or may affect safety.

When choosing feedback on E-mail, anonymity is guaranteed if a personal E-mail outside the corporate network is specified.
Unconditional anonymity is guaranteed if the "Notify in the app" option is selected.
           
- **Who has access to the message:** *Only Safety Manager and his/her deputee.*
- **Who knows the name and email of a sender:** *Nobody knows.*
- **Protection against administrative penalties:** *Do not provide protection from  administrative penalties.*           
           
## Confidential

Ensures an employee’s right to confidential reporting of a situation that affects or may affect safety.

When choosing feedback on E-mail, confidentiality is guaranteed if a personal E-mail outside the corporate network is specified.
Unconditional confidentiality is guaranteed if the "Notify in the app" option is selected.
           
- **Who has access to the message:** *Only Safety Manager and his/her deputee.*
- **Who knows the name and email of a sender:** *Only Safety Manager and his/her deputee.*
- **Protection against administrative penalties:** *May protect the employee from administrative penalties.*                
           
## Voluntary

Ensures an employee’s right to to open and unhindered reporting of a situation that affects or may affect flight safety.
           
- **Who has access to the message:** *Safety Manager, his/her deputee, Safety Representatives and those managers who are given appropriate right.*
- **Who knows the name and email of a sender:** *Safety Manager, his/her deputee and Safety Representatives who are given appropriate right.*
- **Protection against administrative penalties:** *As usual, protects the employee from administrative penalties.*           
           
           
## Mandatory

Are to be used for fulfillment employee's obligation to inform the airline about cases that require notification. Lists of mandatory safety related occurrences are defined in airline guidelines.
           
- **Who has access to the message:** *Safety Manager, his/her deputee, Safety Representatives and those managers who are given appropriate right.*
- **Who knows the name and email of a sender:** *Safety Manager, his/her deputee and Safety Representatives who are given appropriate right.*
- **Protection against administrative penalties:** *May protect the employee from administrative penalties.*           