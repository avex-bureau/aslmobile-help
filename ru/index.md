---
layout: default
lang: ru
ref: home
---

<div class="home">
  <div class="video-container"><iframe width="100%" src="https://www.youtube.com/embed/lxBCfKITako" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
  {% assign pages=site.pages | where:"lang", page.lang %}
  <ul class="post-list">
    {% for page in pages %}
      {% if page.ref != 'home' %}
        <li>
          <a class="post-link" href="{{ page.url | prepend: site.baseurl }}">{{ page.title }}</a>
        </li>
      {% endif %}
    {% endfor %}
  </ul>
</div>
